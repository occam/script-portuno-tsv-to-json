ROOT=$(readlink -f $(dirname $0))

export PYENV_ROOT=${ROOT}/pyenv
export PATH=$PYENV_ROOT/bin:$PATH
eval "$(pyenv init -)"

pyenv local 3.3.3
python ${ROOT}/tsvtojson.py
mv *.json objects/${OCCAM_OBJECT_INDEX}/.
mv objects/${OCCAM_OBJECT_INDEX}/object.json .
