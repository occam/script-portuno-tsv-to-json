export PYENV_ROOT=`pwd`/pyenv
export PATH=$PYENV_ROOT/bin:$PATH
eval "$(pyenv init -)"

pyenv install 3.3.3 -f
pyenv local 3.3.3
pip install numpy
export CPATH=$CPATH:`python-config --prefix`/include
export LIBRARY_PATH=$LIBRARY_PATH:`python-config --prefix`/lib
