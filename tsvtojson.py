import csv
import numpy
import json
import io
from occam import Occam

object = Occam.load()
configuration = object.configuration("General")


inputs = object.inputs()
input_path = 'latest.tsv'

if len(inputs) > 0:
  files = inputs[0].files()

  if len(files) > 0:
    input_path = inputs[0].files()[0]

data_source=input_path
col_ignore=[int(x) for x in configuration['col_ignore'].split(',')]
plot_type=configuration['plot_type']
plot_mode=configuration['plot_mode']
delimiter='\t'
plot_to_axis_sep='-'
axis_name_sep='_'

tsvdata = numpy.array(list(csv.reader(open(data_source, 'r'), delimiter=delimiter)))

col=len(tsvdata[1,:])
row=len(tsvdata[:,1])

all_col=range(col-1)[::1]
valid_col=list(set(all_col)-set(col_ignore))

def long_substr(data):
	substr = ''
	if len(data) > 1 and len(data[0]) > 0:
		for i in range(len(data[0])):
			for j in range(len(data[0])-i+1):
				if j > len(substr) and all(data[0][i:i+j] in x for x in data):
					substr = data[0][i:i+j]
	return substr

lcs=long_substr(tsvdata[0,valid_col]);

methods = []
measurements = []
i = 0

for colheader in tsvdata[0,valid_col]:
	methods.append(colheader[colheader.index(lcs)+len(lcs):colheader.index(plot_to_axis_sep)])
	measurements.append(colheader[colheader.index(plot_to_axis_sep)+1:len(colheader)])
	i += 1

methods=numpy.unique(methods)
measurements=numpy.unique(measurements)

def find_colpos(pattern, data):
	if len(data) < 1 and len(pattern) < 1:
		return 0
	pos = 0
	for colheader in data:
		if colheader.find(pattern) > -1:
			return pos
		pos += 1


data_template = {
		"x": [],
		"y": [],
		"name": "",
		"type": plot_type,
		"mode": plot_mode
	   	}

output_template = {
		"data" : [],
		"layout" : 	{
					"title" : "",
					"xaxis":{
							"title":""
						},
					"yaxis":{
							"title":""
						}
				}
	 	}

for measure1 in measurements:
	for measure2 in measurements:
		if measure1 != measure2:
			output = output_template.copy()
			output["data"] = []
			for method in methods:
				find1 = lcs + method + plot_to_axis_sep + measure1
				find2 = lcs + method + plot_to_axis_sep + measure2
				xpos = find_colpos(find1, tsvdata[0, :])
				ypos = find_colpos(find2, tsvdata[0, :])
				data = data_template.copy()
				data["x"] = tsvdata[1:row, xpos].tolist()
				data["y"] = tsvdata[1:row, ypos].tolist()
				data["name"]=method
				output["data"].append(data)
			chart_title = measure1 + '_vs_' + measure2
			output["layout"]["title"] = chart_title.replace(axis_name_sep, ' ');
			output["layout"]["xaxis"]["title"] = measure1.replace(axis_name_sep, ' ');
			output["layout"]["yaxis"]["title"] = measure2.replace(axis_name_sep, ' ');
			with io.open(chart_title + '.json', 'w', encoding='utf-8') as f:
				json.dump(output, f, sort_keys=True, indent=4, ensure_ascii=False)
