import os
import subprocess
import json
import shutil

from occam import Occam

# Get occam object metadata
object = Occam.load()
configuration = object.configuration("General")

# Gather paths
scripts_path    = os.path.dirname(__file__)
binary_path     = os.path.join(scripts_path, "local", "sst-5.0", "bin")
binary          = os.path.join(binary_path, "sst")
portuno_path    = os.path.join(scripts_path, "sst-5.0.1", "sst", "elements", "sst_Portuno")
job_path        = os.getcwd()

tsv_path = os.path.join(scripts_path, 'latest.tsv')

# Look at inputs
inputs = object.inputs("text/tab-separated-values")

# Gather the file as input
if len(inputs) > 0:
  tsv = inputs[0]
  files = tsv.files()

  if len(files) > 0:
    tsv_path = files[0]
  else:
    tsv_path = os.path.join(tsv.volume(), 'latest.tsv')

print("Copying latest.tsv from %s" % (tsv_path))
shutil.copyfile(tsv_path, os.path.join(job_path, 'latest.tsv'))

# This command will generate the graph for the given SST input file.
args = ["/bin/bash",
        os.path.join(scripts_path, "run.sh")]

# Form command line
command = ' '.join(args)

# Tell OCCAM how to run SST
Occam.report(command)
